package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentServiceImpl implements StudentService {
    StudentDao studentDao;

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public Student findStudentById(String id) {
        for (Student student: this.studentDao.findAll()
             ) {
            if (student.getStudentId().equals(id)){
                return student;
            }
        }
        //9.2
        throw new NoDataException();
    }

    @Override
    public List<Student> findStudentByPartOfId(String id) {
        List<Student> output = new ArrayList<>();
        for (Student student: this.studentDao.findAll()
                ) {
            if (student.getStudentId().indexOf(id) != -1){
                output.add(student);
            }
        }
        //9.3
        throw new OutputArrayIsZero();
    }


    @Override
    public double getAverageGpa() {
        double total = 0;
        if (this.studentDao.findAll().size() == 0 ) {
            throw new DataDividedByZero();
        }
        for (Student student: this.studentDao.findAll()
        ) {
            total += student.getGpa();

        }
        return total/this.studentDao.findAll().size();
    }

    // ownStudent
    @Override
    public Student findOwnStudentById(String id) {
        for (Student student: this.studentDao.findOwnStudent()
        ) {
            if (student.getStudentId().equals(id)){
                return student;
            }
        }
        return null;
    }

    @Override
    public List<Student> findOwnStudentByPartOfId(String id) {
        List<Student> output = new ArrayList<>();
        for (Student student: this.studentDao.findOwnStudent()
        ) {
            if (student.getStudentId().indexOf(id) != -1){
                output.add(student);
            }
        }
        return output;
    }

    @Override
    public double getAverageGpaOwnStudent() {
        double total = 0;
        for (Student student: this.studentDao.findOwnStudent()
        ) {
            total += student.getGpa();

        }
        return total/this.studentDao.findOwnStudent().size();
    }
}
